# [3 Ugly Truths to Designing Clean Apps](https://appdev-coe.gitlab.io/3-ugly-truths-to-designing-clean-apps)

 - [Lightning talk session for 3 Ugly Truths to Designing Clean Apps](https://appdev-coe.gitlab.io/3-ugly-truths-to-designing-clean-apps/lightning-talk.html)

 - [Full session for 3 Ugly Truths to Designing Clean Apps](https://appdev-coe.gitlab.io/3-ugly-truths-to-designing-clean-apps/full-talk.html)


## Abstract:
There are so many ugly things we’ve seen in designs during our developer lifetime, how can you face them like you face your fears? The problem with ugly truth is that it’s hard to swallow and even harder to change the behavior behind it. These behaviors have led to design habits that breeds very dirty applications. In this session we’ll propose three very, very ugly truths that developers need to face. We promise they’re not going to be easy to face, but sometimes tough love is just what we need to achieve the next level in clean application designs.

 - You have to hug your user! (involving user is a must)

 - Good Code != Good Design (code does not overrule user input)

 - Change your religion to simplicity


[![Cover Slide](https://gitlab.com/appdev-coe/3-ugly-truths-to-designing-clean-apps/raw/master/cover.png)](https://appdev-coe.gitlab.io/3-ugly-truths-to-designing-clean-apps)
